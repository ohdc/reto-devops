# ---------------------------------------------------------------------------------------------------------------------

terraform {
  required_version = ">= 0.12"
}

# ---------------------------------------------------------------------------------------------------------------------

resource "null_resource" "dependency_getter" {
  triggers = {
    instance = join(",", var.dependencies)
  }
}

# ---------------------------------------------------------------------------------------------------------------------
# Se crea el rol
# Se usa `k8s-ns-roles`
# ---------------------------------------------------------------------------------------------------------------------

module "namespace_roles" {
  source = "../k8s-ns-roles"

  #namespace   = var.create_resources ? kubernetes_namespace.namespace[0].id : ""
  namespace   = var.create_resources
  #namespace   = "default"
  labels      = var.labels
  annotations = var.annotations

  create_resources = var.create_resources
  dependencies     = var.dependencies
}
