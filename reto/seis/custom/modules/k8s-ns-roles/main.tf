# ---------------------------------------------------------------------------------------------------------------------

terraform {
  required_version = ">= 0.12"
}

# ---------------------------------------------------------------------------------------------------------------------

resource "null_resource" "dependency_getter" {
  triggers = {
    instance = join(",", var.dependencies)
  }
}

resource "kubernetes_role" "rbac_role_access_read_only" {
  count      = var.create_resources ? 1 : 0
  depends_on = [null_resource.dependency_getter]

  metadata {
    #name        = "${var.namespace}-access-read-only"
    name        = "default-access-read-only"
    #namespace   = var.namespace
    namespace   = "default"
    labels      = var.labels
    annotations = var.annotations
  }

  rule {
    api_groups = [""]
    resources  = ["pods"]
    verbs      = ["get", "list", "watch"]
  }
}

